Demo: https://kanrens-demo-372.surge.sh/

Created when I tried to create something like puzzlescript but as simple as possible.

Turns out it was easy with micro/minikanren (I savaged this implementation). It's more general-purpose than the P:S engine.

## How to run

```
git clone --recursive <this url> # or use git submodule init
npm install
npm run dev
```
